import React from 'react';

const RightArrow = (props) => {
  return (
     <div className="arrow-right" onClick={props.goToNextSlide}>
       <div className="arrow"></div>
     </div>
  );
}

export default RightArrow;
