import React, { Component } from "react";
import Slide from "./Slide";
import LeftArrow from "./ArrowLeft";
import RightArrow from "./ArrowRight";
import MobileWork from "./MobileWork";
import { SubNav } from "./SubNav";

export default class Work extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contents: [
        {
          id: 1,
          image: "./img/jose2.jpg",
          mobileImg: "./img/jose.jpg",
          title: "José Palma",
          tool: "Design/WordPress/PHP/SCSS",
          txt:
          "This project is for a final website product. I worked as a web designer and developer for my friend. I designed and created this site from scratch. I made this with Wordpress. This site is mobile friendly.",
            url:"https://jose.palmas.se/",
            imgPosition: "changeImg"
        },
         {
          id: 2,
          image: "./img/rcfg.jpg",
          mobileImg: "./img/rcfg_mobile.jpg",
          minititle_1: "Research Center for",
          title: "Financial Gerontology",
          tool: "Design/WordPress/PHP/CSS",
          txt:
          "This project is for a final website product. I worked as a web designer and developer for university professor in Japan. I designed and created this site from scratch. I made this with Wordpress. This site is mobile friendly.",
            url:"https://rcfg.keio.ac.jp"
        },
        {
          id: 3,
          image: "./img/raktfram.jpg",
          mobileImg: "./img/raktfram.png",
          title: "Raktfram",
          tool: "Design/HTML,SCSS/PHP/Javascript",
          txt:
          "This project is for a final website product.  I'm working as a web designer and developer for my friend which has consulting firm in Japan. I focused on a simple design and animation. I’m satisfied with this website design. I will continue to develop this website. I created a mobile friendly version first and then built the desktop version. ",
            url:"https://raktfram.co.jp/",
            imgPosition: "changeImg"
        },
        {
          id: 4,
          image: "./img/recipie.jpg",
          mobileImg: "./img/recipiemobile.jpg",
          title: "Recipie",
          tool: "Design/SCSS/React.js/Firebase",
          txt:
          "This is master thesis project I developed with my classmate. At this application  you can create an account, write and save your own recipes, and check other users' recipes. We have created the application with the javascript library React.js and used Firebase as a database to store all information.",
            url:"https://recipie-d2f08.firebaseapp.com/"
        },
         {
          id: 5,
          image: "./img/ca.jpg",
          mobileImg: "./img/ca_mobile.jpg",
          title: "Cabinattendant.jp",
          tool: "Design/HTML,SCSS/PHP/Javascript",
          txt:
          "This project is for a final website product. I worked as a web designer and developer. I created a logo and this site from scratch. I had focused on a design and animation. This site is also responsive (mobile friendly).",
            url:"https://www.cabinattendant.jp/",
            imgPosition: "changeImg"
            
        },
         {
          id: 6,
          image: "./img/bonaibon.jpg",
          mobileImg: "./img/bonaibon_mobile.png",
          title: "Bon Aibon",
          tool: "Design/HTML,SCSS/Javascript",
          txt:
          "This project is for a final website product.  I worked as a web designer and developer for my friend who is working as a Japanese pastry chef in Sweden. I'm so happy that I could help her work. This site is mobile friendly.",
            url:"https://www.bon-aibon-sweets.com/"
        },
         {
          id: 7,
          image: "./img/jfga.jpg",
          mobileImg: "./img/jfga_mobile.jpg",
          title: "Japan Financial",
          minititle: " Gerontology Association",
          tool: "Design/Wordpress/CSS/PHP",
          tool2: "Javascript/Photoshop/Illustrator",
          txt:
            "This project is for a final website product. I worked as a web designer and developer for university professor in Japan. I designed and created this site from scratch.  I made this with Wordpress which is my own template. This project makes me a really good experience because it has good benefits of using Wordpress  and one of them is that client will be able to add and update posts own themself. This site is mobile friendly.",
            url:"https://www.fga.jp/",
            imgPosition: "changeImg"
        },
        {
          id: 8,
          image: "./img/the_white_stripes2.jpg",
          mobileImg: "./img/white_stripes.jpg",
          title: "The White Stripes ",
          tool: "Design/HTML,SCSS/Jekyll/Javascript",
          txt: "As a School project with a group of 3 people and with only 2 weeks to create a website. Our team had a good image already made on how to make this page interesting, we focused on design and animation. This site made with Jekyll and to add the modern touch we integrated a horizontal scroll.  This project gave me a lot of inspiration and how animation nowadays plays an important role on websites. You want to catch the visitors attention and animation can do just that. This project made me comfortable with animation, sass and Jekyll.",
          url: "https://angry-heisenberg-302cd5.netlify.com"
        },
        {
          id: 9,
          image: "./img/wangimationpage.jpg",
          mobileImg: "./img/wangimation1.jpg",
          title: "Wangimation ",
          minititle: "Wedding Photography",
          tool: "Design/HTML,CSS",
          tool2: "Jquery/PHP/MySQL/Photoshop",
          txt:
            "As a school project, I got a request for website from my classmate which was to create a subscription landing page for a wedding photography company. One of the main points was a subscription service for the company. I designed and created this site from zero. All of the pictures on the website are taken by me when I worked as a photographer. I learned new things and one of them was how to create a database which was really interesting to get a grasp of. This website is also a responsive site meaning mobile and tablet friendly.",
          url: "https://upbeat-bohr-6eecf5.netlify.com",
          imgPosition: "changeImg"
        },
        {
          id: 10,
          image: "./img/sakura2.jpg",
          mobileImg: "./img/sakurastyle.jpg",
          title: "SAKURA STYLE ",
          tool: "Design/HTML,CSS/Bootstrap/PHP",
          tool2: "Javascript/Photoshop/Illustrator",
          txt:
            "A project for a final website product. I got an order from my client who has started a new wedding photography company. Through working closely with them I created this website. This project gave me really good experience because I could deliver a real product for my first client. This website is live. I will continue to take care and maintenain this website. This site is also responsive (mobile friendly).",
          url: "https://sakurastyle-cancun.com/"
        },
        {
          id: 11,
          image: "./img/bys.jpg",
          mobileImg: "./img/bys1.jpg",
          title: "Build Your Site",
          tool: "Design/HTML,CSS/Javascript",
          txt:
            "School project, this was a group work of 4 people and with only 3 weeks to create a website. Our team had a good strategy using an agile/scrum method to manage this project. We got a website requirements from our teacher and created a generator on the website with a function which made it possible to download the html and css codes. This gave me great insight in how effective teamwork can work with a good strategy and the importance of communication with each other.",
          url: "https://tocico.github.io/bys.github.io/",
          imgPosition: "changeImg"
        },
        {
          id: 12,
          image: "./img/pizzaproject.jpg",
          mobileImg: "./img/pizza1.jpg",
          title: "Mas Bene",
          tool: "Design/HTML,CSS/Bootstrap/PHP/Javascript",
          tool2: "Photoshop/Illustrator",
          txt:
            "As a school project, this was a 3 hours pizza project. I designed and created this website from scratch. Logotype and picture editing was also done by me. It was a good opportunity to create a simple website with a short deadline.",
          url: "https://tocico.github.io/pizzaprojekt/"
        },
        {
          id: 13,
          image: "./img/csslayout.jpg",
          mobileImg: "./img/csslayout1.jpg",
          title: "CSS Layout Learning",
          tool: "Design/HTML,CSS",
          txt:
            "As a school project, this was made in groups and we had 3 weeks to build this website. It was our first time to create a website with our fresh html and css skills. This website is with information about box model layout, flexbox, grids, table, positioning and floats. I mainly worked with design, layout, web interface, box model page and table page. We created a mobile friendly version first with progressive advancement and then built the desktop version. This site is mobile friendly.",
          url: "https://tocico.github.io/csslayoutlearning.github.io/index.html"
        }
      ],
      currentIndex: 0,
      translateValue: 0
    };
  }

  goToPrevSlide = () => {
    if (this.state.currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: prevState.translateValue + this.slideWidth()
    }));
    console.log(this.state.translateValue);
  };

  goToNextSlide = () => {
    if (this.state.currentIndex === this.state.contents.length - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0
      });
    }
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth()
    }));
    console.log(this.state.translateValue);
  };

  slideWidth = () => {
    return document.querySelector(".container-work").clientWidth;
  };

  render() {
    return (
      <div className="container work" id="work">
        <SubNav navTitle={"My work"} />

        <div className="desktop-work">
          <div
            className="slider-wrapper"
            style={{
              transform: `translateX(${this.state.translateValue}px)`,
              transition: " box-shadow 0.5s ease-in-out"
            }}
          >
            {this.state.contents.map((content, i) => (
              <Slide key={i} contents={content} />
            ))}
          </div>
          <LeftArrow goToPrevSlide={this.goToPrevSlide} />

          <RightArrow goToNextSlide={this.goToNextSlide} />
        </div>
        <div className="mobile-work">
          <h2 className="mobile-title">My works</h2>
          {this.state.contents.map((content, i) => (
            <MobileWork key={i} contents={content} />
          ))}
        </div>
      </div>
    );
  }
}
