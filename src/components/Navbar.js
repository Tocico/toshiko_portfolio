import React ,{useState} from "react";
import  styles from '../styles/nav-bar.module.scss'
import AnchorLink from 'react-anchor-link-smooth-scroll'


function Navbar() {
    const [showMenu, setShowMenu] = useState(false);
    const menuActive = showMenu ? `${styles.open}` : '';
    const triggerActive = showMenu ? `${styles.open}` : '';
    const arrowActive = showMenu ? `${styles.active}` : '';
  return (

    <header>
      <div className={styles.logo}>
        <a href="/">
          <img src="./img/logo.png" alt="Toshikos logo" />
        </a>
      </div>
      <div className="wrapper">
      <div className={`${styles.menuTrigger} ${triggerActive} ${arrowActive}`} onClick={()=> setShowMenu(!showMenu)}>
        <span></span>
        <span></span>
        <span></span>
    </div>
    <nav className={`${menuActive}`}>
            <ul>
              <li>
              <AnchorLink href='#home' onClick={()=> setShowMenu(!showMenu)}>Home</AnchorLink>
              </li>
              <li>
              <AnchorLink href='#about' onClick={()=> setShowMenu(!showMenu)}>About</AnchorLink>
              </li>
              <li>
              <AnchorLink href='#work' onClick={()=> setShowMenu(!showMenu)}>Work</AnchorLink>
              </li>
              <li>
              <AnchorLink href='#skill' onClick={()=> setShowMenu(!showMenu)}>Skill</AnchorLink>
              </li>
              <li>
              <a href="mailto:toshikokuno17&#64;gmail.com" onClick={()=> setShowMenu(!showMenu)}>Contact</a>
              </li>
            </ul>
        </nav>
         <div className="overlay"></div>
      </div>
    </header>
  );
};

export default Navbar;
