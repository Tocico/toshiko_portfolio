import React from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import { useSpring, animated } from "react-spring";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";

library.add(fab, faEnvelope);

const Home = () => {
  const easeOutFromLeft = useSpring({
    from: {
      opacity: 0,
      transform: "translateX(-50px)",
      transition: "all 1s ease-out"
    },
    opacity: 1,
    transform: "translateX(0)"
  });
  const easeOutFromRight = useSpring({
    from: {
      opacity: 0,
      transform: "translateX(50px)",
      transition: "all 1s ease-out"
    },
    opacity: 1,
    transform: "translateX(0)"
  });
  return (
    <div className="container container-home" id="home">
      <aside>
        <div className="fixed">
          <a
            href="https://www.linkedin.com/in/toshikokuno/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon className="icon" icon={["fab", "linkedin"]} />
          </a>
          <a
            href="https://www.instagram.com/t0ci/?hl=ja"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon className="icon" icon={["fab", "instagram"]} />
          </a>
          <a
            href="https://github.com/Tocico"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FontAwesomeIcon className="icon" icon={["fab", "github"]} />
          </a>
          <a href="mailto:kunotoshiko17&#64;gmail.com">
            <FontAwesomeIcon className="icon" icon={faEnvelope} />
          </a>
        </div>
      </aside>

      <section className="container-home-middle desktop">
        <animated.div style={easeOutFromLeft}>
          <h1>Toshiko Kuno</h1>
          <h2>A Japanese based in Stockholm.</h2>
        </animated.div>
        <div>
          <AnchorLink className="arrow" href="#about">
            <svg
              version="1.1"
              id="Layer_1"
              x="0px"
              y="0px"
              viewBox="150 -150 500 500"
            >
              <path
                className="st0 path"
                d="M250.5,143.5L253.5,280.5L223.5,250.5L268.5,250.5"
              ></path>
            </svg>
          </AnchorLink>
        </div>
      </section>

      <section className="container-home-right desktop">
        <animated.div style={easeOutFromRight} className="bg"></animated.div>
      </section>

      <section className="mobile marginTop">
        <div className="mobile-bg">

          <div className="textOnPic">
            <h1>Toshiko Kuno</h1>
            <h2>A Japanese based in Stockholm.</h2>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
