import React from 'react'


const MobileWork = ({contents})=>{
   return(
      <div className="container-work">
            <img src={contents.mobileImg} alt={contents.title} className={contents.imgPosition}/>
            <div className="work-right">
                <h3 className="title">{contents.title}<div className="minitxt">{contents.minititle}</div>
                </h3>
                <h4>{contents.tool}<br />{contents.tool2}</h4>
                <p className="text">{contents.txt}</p>
                <div className="checkbtn">  {!contents.url? <div className="comingSoon">COMING SOON</div> :  <a href={contents.url} target="_blank" rel="noopener noreferrer">
              CHECK THIS SITE
            </a> }
                </div>
            </div>
      </div>

   );
}

export default MobileWork
