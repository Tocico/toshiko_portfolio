import React from "react";

const Slide = ({ contents }) => {
  return (
    <div className="container-work">
      <div className="container-work-box" id={contents.class}>
        <img src={contents.image} alt={contents.title} />
        <div className="work-right" id={contents.id}>
  <div className="nr">{contents.id >= 10 ? "#" + contents.id : "#0" + contents.id}</div>
          <h3 className="title">
            <div className="minitxt">{contents.minititle_1}</div>
            {contents.title}
            <div className="minitxt">{contents.minititle}</div>
          </h3>
          <h4>
            {contents.tool}
            <br />
            {contents.tool2}
          </h4>
          <div className="line"></div>
          <p className="text">{contents.txt}</p>
          <div className="checkbtn">
            {!contents.url? <div className="comingSoon">COMING SOON</div> :  <a href={contents.url} target="_blank" rel="noopener noreferrer">
              CHECK THIS SITE
            </a> }
           
          </div>
        </div>
      </div>
     </div> 
  );
};

export default Slide;
