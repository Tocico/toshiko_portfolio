import React, { useState } from "react";
import VisibilitySensor from "react-visibility-sensor";
import posed from "react-pose";
import { SubNav } from "./SubNav";

const AnimatedDiv = posed.div({
  visible: {
    opacity: 1,
    transition: {
      ease: "easeInOut"
    },
    y: 0
  },
  hidden: {
    opacity: 0,
    y: 40
  }
});

function Animated({ inView, children }) {
  return (
    <AnimatedDiv
      pose={inView ? "visible" : "hidden"}
      className="container container-about "
      id="about"
    >
      {children}
    </AnimatedDiv>
  );
}

function AnimateOnVisible({ children }) {
  const [hasAnimated, setHasAnimated] = useState(false);
  const onVisibilityChange = isVisible => {
    if (isVisible) {
      setHasAnimated(true);
    }
  };
  return (
    <VisibilitySensor onChange={onVisibilityChange}>
      {({ isVisible }) => {
        const isContentInView = isVisible || hasAnimated;
        return <Animated inView={isContentInView}>{children}</Animated>;
      }}
    </VisibilitySensor>
  );
}

const About = () => {
  return (
    <div>
      <div className="container-about-box desktop">
        <SubNav navTitle={"About"} />
        <AnimateOnVisible>
          <div className="left">
            <div className="container-about-bg">
              <img src="./img/Toshiko.jpg" alt="toshiko" className="toshiko" />
            </div>
          </div>
          <div className="right elem">
            <h3>Hej</h3>
            <p>
              My name is Toshiko Kuno. I am ambitious and creative with great
              passion for design, photography and food. I am a fresh graduated 
              Front-end developer. I enjoy learning new things. Future plans : a great web
              developer.
            </p>
            <div className="button">
              <a href="mailto:kunotoshiko17&#64;gmail.com">contact me</a>
            </div>
          </div>
        </AnimateOnVisible>
      </div>
      <div className="container-about-box mobile">
        <SubNav navTitle={"About"} />
        <div className="container container-about" id="about">
          <div className="left">
            <div className="container-about-bg">
              <img src="./img/Toshiko.jpg" alt="toshiko" className="toshiko" />
            </div>
          </div>
          <div className="right elem">
            <h3>Hej</h3>
            <p>
              My name is Toshiko Kuno. I am ambitious and creative with great
              passion for design, photography and food. I am currently in my
              second year of Front-end development studies at Nackademin in
              Stockholm. I enjoy learning new things. Future plans : a great web
              developer.
            </p>
            <div className="button">
              <a href="mailto:kunotoshiko17&#64;gmail.com">contact me</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default About;
