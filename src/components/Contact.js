import React, {useState} from "react";
import {SubNav} from './SubNav';
import VisibilitySensor from "react-visibility-sensor";
import posed from "react-pose";

const AnimatedDiv = posed.div({
  visible: {
    opacity: 1,
    transition: {
      ease: "easeInOut",
      duration: 1000
    },
    x:0,
  },
  hidden: {
    opacity: 0,
    x: 50
  }
});


function Animated({ inView, children }) {
  return (
    <AnimatedDiv
      pose={inView ? "visible" : "hidden"}
      className="contact-right-txt"
    >
      {children}
    </AnimatedDiv>
  );
}

function AnimateOnVisible({ children }) {
  const [hasAnimated, setHasAnimated] = useState(false);
  const onVisibilityChange = isVisible => {
    if (isVisible) {
      setHasAnimated(true);
    }
  };
  return (
    <VisibilitySensor onChange={onVisibilityChange}>
      {({ isVisible }) => {
        const isContentInView = isVisible || hasAnimated;
        return <Animated inView={isContentInView}>{children}</Animated>;
      }}
    </VisibilitySensor>
  );
}

const Contact = () => {
  return (
    <div className="container contact" id="contact">
      <SubNav navTitle={'Contact'}/>
      <div className="contact-container-box">
        <div className="contact-left">
          <div className="contact-bg"></div>
        </div>
        <div className="contact-right">
          <div className="box"></div>
          <AnimateOnVisible>
            Contact Me
            <div className="contact-mail">
              <a href="mailto:kunotoshiko17@gmail.com">kunotoshiko17@gmail.com</a>
            </div>
          </AnimateOnVisible>
        </div>
      </div>
    </div>
  );
};

export default Contact;
