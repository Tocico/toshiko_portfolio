import React from "react";

export const SubNav = ({ navTitle }) => {
  return (
    <div className="navBar">
      <h1>{navTitle}</h1>
    </div>
  );
};
