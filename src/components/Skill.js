import React from 'react'
import ProgrammingSkills from './ProgrammingSkills'
import {SubNav} from './SubNav'


const Skill = ()=>{
   return(
      <div className='container skill' id="skill">
         <SubNav navTitle={'Skills'} />
         <h2 className="mobile-title">Skills</h2>
        <ProgrammingSkills />
      </div>
   );
}

export default Skill
