import React from 'react';

const LeftArrow = (props) => {
  return (
      <div className="arrow-left" onClick={props.goToPrevSlide}>
       <div className="arrow"></div>
     </div>
  );
}

export default LeftArrow;