import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";

library.add(fab, faEnvelope);

const Footer = () => {
  return (
    <footer className="footer">
      <div className="mobile">
        <a
          href="https://www.linkedin.com/in/toshikokuno/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FontAwesomeIcon className="icon" icon={["fab", "linkedin"]} />
        </a>
        <a
          href="https://www.instagram.com/t0ci/?hl=ja"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FontAwesomeIcon className="icon" icon={["fab", "instagram"]} />
        </a>
        <a
          href="https://github.com/Tocico"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FontAwesomeIcon className="icon" icon={["fab", "github"]} />
        </a>
        <a href="mailto:kunotoshiko17&#64;gmail.com">
            <FontAwesomeIcon className="icon" icon={faEnvelope} />
          </a>
      </div>
      <div className="copyright">©2019ToshikoKuno</div>
    </footer>
  );
};

export default Footer;
